/**
 * Created by steve on 27/10/2016.
 */
/////////////////////////////////////////
// ScrollMagic Animation Framework     //
// Developer: Steven Willett           //
// Company: @Koozai https://koozai.com //
/////////////////////////////////////////

$(document).ready(function() { // document ready

    $(window).resize(function() { // Window Resize

        // Reload page to recalibrate CSS widths etc.
        $(window).on('resize', function(){
            location.reload();
        });

        // Window Sizes
        var vw = $(window).width();
        var vh = $(window).height();
        var containerWidth = $('.container-fluid').width();

        // init controller
        var controller = new ScrollMagic.Controller();

        // Timelines
        var rotate360Animation = new TimelineMax();
        var cubesSlideInFromCornersAnimation = new TimelineMax();
        var curtainSlideInHorizontalAnimation = new TimelineMax();
        var curtainSlideInVerticalAnimation = new TimelineMax();

        // Animation: Rotate 360
        rotate360Animation
            .to('#scene_1 .animation-type', 0.5, {rotation: 360, ease: Power0.easeNone})
        ;
        var rotate = new ScrollMagic.Scene({
            triggerElement: "#scene_1",
            triggerHook: 0,
            reverse: true,
            duration: '100%'
        })
            .setTween(rotate360Animation)
            .setPin('#scene_1')
            .addIndicators({name: "Rotate 360"}) // Enable "addIncators" plugin or comment out.
            .addTo(controller);


        // Animation: 4 Cubes, slide in from each viewport corner respectively (Top Left, Top Right, Bottom Left, Bottom Right)
        cubesSlideInFromCornersAnimation
            .fromTo('#scene_2 .cube .top-left', 0.5, {x: '-100%', y: '-100%', ease: Power0.easeNone}, {x: '0%', y: '0%', ease: Power0.easeNone},0)
            .fromTo('#scene_2 .cube .top-right', 0.5, {x: '100%', y: '-100%', ease: Power0.easeNone}, {x: '0%', y: '0%', ease: Power0.easeNone},0)
            .fromTo('#scene_2 .cube .bottom-left', 0.5, {x: '-100%', y: '100%', ease: Power0.easeNone}, {x: '0%', y: '0%', ease: Power0.easeNone},0)
            .fromTo('#scene_2 .cube .bottom-right', 0.5, {x: '100%', y: '100%', ease: Power0.easeNone}, {x: '0%', y: '0%', ease: Power0.easeNone},0)
        ;
        var diagonalSlideIn = new ScrollMagic.Scene({
            triggerElement: "#scene_2",
            triggerHook: 0,
            reverse: true,
            duration: '100%'
        })
            .setTween(cubesSlideInFromCornersAnimation)
            .setPin('#scene_2')
            .addIndicators({name: "Slide-In Cubes"}) // Enable "addIncators" plugin or comment out.
            .addTo(controller);


        // Animation: Horizontal Slide in (Left & Right) Panel curtains
        curtainSlideInHorizontalAnimation
            .fromTo('#scene_3 .horizontal-curtain .left-curtain', 0.5, {x: '-100%', ease: Power0.easeNone}, {x: '0%', ease: Power0.easeNone},0)
            .fromTo('#scene_3 .horizontal-curtain .right-curtain', 0.5, {x: '100%', ease: Power0.easeNone}, {x: '0%', ease: Power0.easeNone},0)
        ;
        var curtainsHorizontalSlideIn = new ScrollMagic.Scene({
            triggerElement: "#scene_3",
            triggerHook: 0,
            reverse: true,
            duration: '100%'
        })
            .setTween(curtainSlideInHorizontalAnimation)
            .setPin('#scene_3')
            .addIndicators({name: "Horizontal Slide-In Curtain Panels"}) // Enable "addIncators" plugin or comment out.
            .addTo(controller);


        // Animation: Vertical Slide in (Top & Bottom) Panel curtains
        curtainSlideInVerticalAnimation
            .fromTo('#scene_4 .vertical-curtain .top-curtain', 0.5, {y: '-100%', ease: Power0.easeNone}, {y: '0%', ease: Power0.easeNone},0)
            .fromTo('#scene_4 .vertical-curtain .bottom-curtain', 0.5, {y: '100%', ease: Power0.easeNone}, {y: '0%', ease: Power0.easeNone},0)
        ;
        var curtainsVerticalSlideIn = new ScrollMagic.Scene({
            triggerElement: "#scene_4",
            triggerHook: 0,
            reverse: true,
            duration: '100%'
        })
            .setTween(curtainSlideInVerticalAnimation)
            .setPin('#scene_4')
            .addIndicators({name: "Vertical Slide-In Curtain Panels"}) // Enable "addIncators" plugin or comment out.
            .addTo(controller);

    }).resize(); // END OF: window resize

}); // END OF: document ready
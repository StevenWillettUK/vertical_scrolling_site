/**
 * Created by steve on 14/10/2016.
 */
$(document).ready(function(){

    // Smooth Scroll
    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });// END OF: Smooth Scroll

    // Navigation
    var menu = $('.main-nav');
    var navLabel = $('.labelTag');
    var navExpand = $('.nav-expand');
    var menuIcon = $('.menu-icon i.rotate');

    navExpand.on('click', function(e) {
        navLabel.toggleClass('labelActive');
        menuIcon.toggleClass('spin180');
        e.preventDefault();
        return false;
    });// END OF: Navigation

});